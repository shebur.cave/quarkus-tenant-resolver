# quarkus-tenant-resolver

Simple quarkus extension, implementing TenantConfigResolver. It uses passed regex string to extract tenant ID from request parameters.

## Why?

Because it was fun task to work on... and can be used later in different projects...

## Usage

Create a new bean from provided implementation. For example, by implementing producer

```java
@ApplicationScoped
public class RegexTenantResolverProducer {

    @ConfigProperty(name = "dev.shebur.tenant-resolver.tenant.source")
    protected TenantSource tenantSource;

    @ConfigProperty(name = "dev.shebur.tenant-resolver.tenant.regex")
    protected String regex;

    @Produces
    @ApplicationScoped
    public RegexTenantResolver regexTenantResolver() {
        return new RegexTenantResolver(
                tenantSource,
                regex
        );
    }

}
```

## Extending

Default implementation will try to extract tenant information and if succeeded will generate a new OidcTenantConfig with extracted tenant Id set.
For more advanced use cases it is possible to extend the default implementation. There are 2 main points for overriding:

* **_createOidcTenantConfig_** - place where OidcTenantConfig is created, thus it can be overwritten to make more customizations to OIDC config. For example, calculate custom auth server URL
* **_onTenantParsed_** - generic callback method, executed when tenant information was successfully parsed. 

Example of ExtendedRegexTenantResolver can be found under integration-tests. In this example, default implementation is improved to:
1. Calculate custom auth server URL, where used realm name equals to tenant name (usecase example: one OIDC server, where each tenant use own realm)
```java
    @Override
    protected Uni<OidcTenantConfig> createOidcTenantConfig(String tenant) {
        if (Strings.isNullOrEmpty(tenant)) {
            return Uni.createFrom().nullItem();
        }

        final OidcTenantConfig config = new OidcTenantConfig();
        config.setAuthServerUrl(oidcUrlTemplate.replace(oidcUrlTenantPlaceholder, tenant));
        config.setTenantId(tenant);

        return Uni.createFrom().item(config);
    }
```
2. Put tenant information into RoutingContext, so it can be easily extracted from endpoints implementations
```java
    @Override
    protected void onTenantParsed(RoutingContext context, String tenant) {
        context.put("tenant", tenant);
    }
```
