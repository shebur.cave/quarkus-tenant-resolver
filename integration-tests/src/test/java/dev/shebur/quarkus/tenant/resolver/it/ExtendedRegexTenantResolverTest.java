package dev.shebur.quarkus.tenant.resolver.it;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import dev.shebur.quarkus.tenant.resolver.RegexTenantResolver;
import dev.shebur.quarkus.tenant.resolver.TenantSource;
import io.quarkus.oidc.OidcRequestContext;
import io.quarkus.oidc.OidcTenantConfig;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.RoutingContext;

@QuarkusTest
public class ExtendedRegexTenantResolverTest {

    static final String OIDC_URL_TEMPLATE = "http://oidc.me/realms/[tenant]";

    static final String OIDC_URL_PLACEHOLDER_TENANT = "[tenant]";

    static final String TENANT_QUERY = "tenant-query";

    RoutingContext context;
    OidcRequestContext<OidcTenantConfig> oidcRequestContext;
    HttpServerRequest request;

    @BeforeEach
    void beforeEach() {
        context = Mockito.mock(RoutingContext.class);
        oidcRequestContext = Mockito.mock(OidcRequestContext.class);
        request = Mockito.mock(HttpServerRequest.class);

        Mockito.doReturn(request).when(context).request();
        Mockito.doReturn("tenant=" + TENANT_QUERY).when(request).query();
    }

    @Test
    void shouldRecordTenantIntoRoutingContext() {
        final RegexTenantResolver resolver = buildRegexTenantResolver(
                TenantSource.QUERY,
                "tenant=([^&]*)"
        );

        resolver.resolve(context, oidcRequestContext);

        /**
         * check that expected tenant is stored in routing context
         */
        Mockito.verify(context).put(ExtendedRegexTenantResolver.CTX_KEY_TENANT, TENANT_QUERY);
    }

    @Test
    void shouldRewriteOidcAuthServerUrl() {
        final RegexTenantResolver resolver = buildRegexTenantResolver(
                TenantSource.QUERY,
                "tenant=([^&]*)"
        );

        Uni<OidcTenantConfig> result = resolver.resolve(context, oidcRequestContext);

        result.subscribe().with(oidcTenantConfig -> {
            oidcTenantConfig.getAuthServerUrl()
                    .ifPresent(authServerUrl -> {
                        Assertions.assertEquals(authServerUrl, "http://oidc.me/realms/tenant-query");
                    });
        });
    }

    @Test
    void shouldNotRewriteOidcAuthServerUrlWhenNoTenantSupplied() {
        Mockito.doReturn("").when(request).query();

        final RegexTenantResolver resolver = buildRegexTenantResolver(
                TenantSource.QUERY,
                "tenant=([^&]*)"
        );

        Uni<OidcTenantConfig> result = resolver.resolve(context, oidcRequestContext);

        result.subscribe().with(oidcTenantConfig -> {
            Assertions.assertNull(oidcTenantConfig);
        });
    }

    static RegexTenantResolver buildRegexTenantResolver(final TenantSource tenantSource, final String regex) {
        return new ExtendedRegexTenantResolver(
                tenantSource,
                regex,
                OIDC_URL_TEMPLATE,
                OIDC_URL_PLACEHOLDER_TENANT
        );
    }
}
