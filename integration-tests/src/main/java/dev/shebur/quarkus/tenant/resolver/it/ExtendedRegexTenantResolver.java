package dev.shebur.quarkus.tenant.resolver.it;

import com.google.common.base.Strings;

import dev.shebur.quarkus.tenant.resolver.RegexTenantResolver;
import dev.shebur.quarkus.tenant.resolver.TenantSource;
import io.quarkus.oidc.OidcTenantConfig;
import io.smallrye.mutiny.Uni;
import io.vertx.ext.web.RoutingContext;

/**
 * Simple extension of RegexTenantResolver that set parsed tenant name into {@link RoutingContext} and
 * create custom oidc tenant config.
 */
public class ExtendedRegexTenantResolver extends RegexTenantResolver {

    public static final String CTX_KEY_TENANT = "tenant";

    final protected String oidcUrlTemplate;

    final protected String oidcUrlTenantPlaceholder;

    public ExtendedRegexTenantResolver(
            TenantSource tenantSource,
            String regex,
            String oidcUrlTemplate,
            String oidcUrlTenantPlaceholder
    ) {
        super(tenantSource, regex);

        this.oidcUrlTemplate = oidcUrlTemplate;
        this.oidcUrlTenantPlaceholder = oidcUrlTenantPlaceholder;
    }

    @Override
    protected void onTenantParsed(RoutingContext context, String tenant) {
        context.put(CTX_KEY_TENANT, tenant);
    }

    @Override
    protected Uni<OidcTenantConfig> createOidcTenantConfig(String tenant) {
        if (Strings.isNullOrEmpty(tenant)) {
            return Uni.createFrom().nullItem();
        }

        final OidcTenantConfig config = new OidcTenantConfig();
        config.setAuthServerUrl(oidcUrlTemplate.replace(oidcUrlTenantPlaceholder, tenant));
        config.setTenantId(tenant);

        return Uni.createFrom().item(config);
    }
}