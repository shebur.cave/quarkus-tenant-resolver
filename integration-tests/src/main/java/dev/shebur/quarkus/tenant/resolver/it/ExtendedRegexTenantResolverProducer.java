package dev.shebur.quarkus.tenant.resolver.it;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import dev.shebur.quarkus.tenant.resolver.TenantSource;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;

@ApplicationScoped
public class ExtendedRegexTenantResolverProducer {

    @ConfigProperty(name = "dev.shebur.tenant-resolver.tenant.source")
    protected TenantSource tenantSource;

    @ConfigProperty(name = "dev.shebur.tenant-resolver.tenant.regex")
    protected String regex;

    @ConfigProperty(name = "dev.shebur.tenant-resolver.oidc-url-template")
    protected String oidcUrlTemplate;

    @ConfigProperty(name = "dev.shebur.tenant-resolver.oidc-url-tenant-placeholder")
    protected String oidcUrlTenantPlaceholder;

    @Produces
    @ApplicationScoped
    public ExtendedRegexTenantResolver extendedRegexTenantResolver() {
        return new ExtendedRegexTenantResolver(
                tenantSource,
                regex,
                oidcUrlTemplate,
                oidcUrlTenantPlaceholder
        );
    }

}
