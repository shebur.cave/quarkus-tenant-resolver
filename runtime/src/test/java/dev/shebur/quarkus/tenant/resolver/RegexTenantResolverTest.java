package dev.shebur.quarkus.tenant.resolver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.quarkus.oidc.OidcRequestContext;
import io.quarkus.oidc.OidcTenantConfig;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.RoutingContext;

@QuarkusTest
public class RegexTenantResolverTest {

    static final String TENANT_HOST = "tenantHost";
    static final String TENANT_PATH = "tenantPath";
    static final String TENANT_QUERY = "tenantQuery";

    RoutingContext context;
    OidcRequestContext<OidcTenantConfig> oidcRequestContext;
    HttpServerRequest request;

    @BeforeEach
    void beforeEach() {
        context = Mockito.mock(RoutingContext.class);
        oidcRequestContext = Mockito.mock(OidcRequestContext.class);
        request = Mockito.mock(HttpServerRequest.class);

        Mockito.doReturn(request).when(context).request();
        Mockito.doReturn(TENANT_HOST + ".localhost").when(request).host();
        Mockito.doReturn("/" + TENANT_PATH + "/api").when(request).path();
        Mockito.doReturn("tenant=" + TENANT_QUERY).when(request).query();
    }

    @Test
    void shouldResolveTenantFromQuery() {
        final RegexTenantResolver resolver = buildRegexTenantResolver(
                TenantSource.QUERY,
                "tenant=([^&]*)"
        );

        Uni<OidcTenantConfig> result = resolver.resolve(context, oidcRequestContext);

        result.subscribe().with(oidcTenantConfig -> {
            oidcTenantConfig.getTenantId().ifPresent(tenantId -> Assertions.assertEquals(tenantId, TENANT_QUERY));
        });
   }

    @Test
    void shouldResolveTenantFromPath() {
        final RegexTenantResolver resolver = buildRegexTenantResolver(
                TenantSource.PATH,
                "^/([^&]*)/"
        );

        Uni<OidcTenantConfig> result = resolver.resolve(context, oidcRequestContext);

        result.subscribe().with(oidcTenantConfig -> {
            oidcTenantConfig.getTenantId().ifPresent(tenantId -> Assertions.assertEquals(tenantId, TENANT_PATH));
        });
    }

    @Test
    void shouldResolveTenantFromHost() {
        final RegexTenantResolver resolver = buildRegexTenantResolver(
                TenantSource.HOST,
                "^([^&.]*)"
        );

        Uni<OidcTenantConfig> result = resolver.resolve(context, oidcRequestContext);

        result.subscribe().with(oidcTenantConfig -> {
            oidcTenantConfig.getTenantId().ifPresent(tenantId -> Assertions.assertEquals(tenantId, TENANT_HOST));
        });
    }

   static RegexTenantResolver buildRegexTenantResolver(final TenantSource tenantSource, final String regex) {
        return new RegexTenantResolver(
                tenantSource,
                regex
        );
   }

}
