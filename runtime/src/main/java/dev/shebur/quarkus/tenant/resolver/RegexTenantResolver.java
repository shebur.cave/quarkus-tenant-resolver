package dev.shebur.quarkus.tenant.resolver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jboss.logging.Logger;

import com.google.common.base.Strings;

import io.quarkus.oidc.OidcRequestContext;
import io.quarkus.oidc.OidcTenantConfig;
import io.quarkus.oidc.TenantConfigResolver;
import io.quarkus.runtime.util.StringUtil;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.RoutingContext;

/**
 * Resolver, used to extract information about tenant in case if multitenancy is supported
 */
public class RegexTenantResolver implements TenantConfigResolver {

    private static final Logger LOG = Logger.getLogger(RegexTenantResolver.class);

    final protected TenantSource tenantSource;
    final protected String regex;

    public RegexTenantResolver(
            final TenantSource tenantSource,
            final String regex
    ) {
        this.tenantSource = tenantSource;
        this.regex = regex;

        LOG.debug("Initialized RegexTenantResolver for source: " + tenantSource.name());
    }

    @Override
    public Uni<OidcTenantConfig> resolve(final RoutingContext context, final OidcRequestContext<OidcTenantConfig> requestContext) {
        // Extract part of request that should contain tenant information
        final String tenantSourceValue = getTenantSourceValue(context.request());

        // Parse tenant value
        final String tenant = parseTenantSourceValue(tenantSourceValue);

        if (StringUtil.isNullOrEmpty(tenant)) {
            LOG.warn( "Tenant value not found");
        } else {
            onTenantParsed(context, tenant);
        }

        return createOidcTenantConfig(tenant);
    }

    /**
     * Use supplied regex string to extract tenant value from tenantSourceValue
     * @param tenantSourceValue
     * @return
     */
    private String parseTenantSourceValue(final String tenantSourceValue) {
        LOG.debugf("Processing source value '{}' with regex '{}'", tenantSourceValue, regex);

        String tenant = null;
        if (!Strings.isNullOrEmpty(regex) && !Strings.isNullOrEmpty(tenantSourceValue)) {
            final Pattern pattern = Pattern.compile(regex);
            try {
                final Matcher matcher = pattern.matcher(tenantSourceValue);
                if (matcher.find()) {
                    tenant = matcher.group(1);
                }
            } catch (Exception ex) {
                LOG.warn("Cannot extract tenant information", ex);
            }
        } else {
            LOG.debug("Parsing not possible. Skipping.");
        }

        return tenant;
    }

    /**
     * Process parsed tenant value
     * @param tenant
     */
    protected void onTenantParsed(RoutingContext context, String tenant) { }

    /**
     * Create OidcTenantConfig instance. Default implementation returns null if no tenant is found, that
     * results in use of default OidcTenantConfig. If tenant value is defined, it will create a new OidcTenantConfig
     * with set tenant id
     * @param tenant
     * @return
     */
    protected Uni<OidcTenantConfig> createOidcTenantConfig(final String tenant) {
        if (Strings.isNullOrEmpty(tenant)) {
            return Uni.createFrom().nullItem();
        }

        final OidcTenantConfig config = new OidcTenantConfig();
        config.setTenantId(tenant);

        return Uni.createFrom().item(config);
    }


    /**
     * Extract value, that should contain tenant information
     * @param request
     * @return
     */
    protected String getTenantSourceValue(final HttpServerRequest request) {
        switch (this.tenantSource) {
            case HOST:
                return request.host();
            case QUERY:
                return request.query();
            case PATH:
                return request.path();
            case NONE:
            default:
                return null;
        }
    }
}