package dev.shebur.quarkus.tenant.resolver;

public enum TenantSource {
    HOST,
    PATH,
    QUERY,
    NONE
}
