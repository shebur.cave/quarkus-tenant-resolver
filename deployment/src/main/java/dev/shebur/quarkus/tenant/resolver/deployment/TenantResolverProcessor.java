package dev.shebur.quarkus.tenant.resolver.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class TenantResolverProcessor {

    private static final String FEATURE = "tenant-resolver";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }
}
